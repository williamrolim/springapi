package com.ex.projeto.api.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.ex.projeto.api.model.enums.TipoLogradouros;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Endereco {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String bairro;
	//@Enumerated(EnumType.STRING)
	private TipoLogradouros logradouro;
	private String nome_rua;
	private String numero;
	private Boolean status;
	
	private String cidade;
	private String estado;
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cliente_id")
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Cliente cliente;
	
	public Endereco() {
	}

	public Endereco(Long id, String bairro, TipoLogradouros logradouro, String nome_rua, String numero, Boolean status,
			String cidade, String estado) {
		super();
		this.id = id;
		this.bairro = bairro;
		this.logradouro = logradouro;
		this.nome_rua = nome_rua;
		this.numero = numero;
		this.status = status;
		this.cidade = cidade;
		this.estado = estado;
	}

	public Endereco(Long id, String bairro, TipoLogradouros logradouro, String nome_rua, String numero, Boolean status,
			String cidade, String estado, Cliente cliente) {
		super();
		this.id = id;
		this.bairro = bairro;
		this.logradouro = logradouro;
		this.nome_rua = nome_rua;
		this.numero = numero;
		this.status = status;
		this.cidade = cidade;
		this.estado = estado;
		this.cliente = cliente;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public TipoLogradouros getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(TipoLogradouros logradouro) {
		this.logradouro = logradouro;
	}

	public String getNome_rua() {
		return nome_rua;
	}

	public void setNome_rua(String nome_rua) {
		this.nome_rua = nome_rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	
}
