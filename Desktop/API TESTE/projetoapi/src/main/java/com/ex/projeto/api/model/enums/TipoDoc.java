package com.ex.projeto.api.model.enums;

public enum TipoDoc {
	CARTEIRA_DE_MOTORISTA ("Carteira de Motorista"), 
	RG("rg"), 
	CPF("cpf");
	
	private final String documento;
	
	private TipoDoc (String documento) {
		this.documento = documento;
	}
	
	public String Documento() {
		return documento;
	}
	
}
