package com.ex.projeto.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ex.projeto.api.model.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Long>{

}
