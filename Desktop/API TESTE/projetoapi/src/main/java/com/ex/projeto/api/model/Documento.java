package com.ex.projeto.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.ex.projeto.api.model.enums.TipoDoc;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Documento implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Enumerated(EnumType.STRING)
	private TipoDoc tipodoc;
	
	private String numero;
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cliente_id")
	//@JsonProperty(access = Access.WRITE_ONLY)
	private Cliente cliente;
	
	public Documento() {
	}
	
	public Documento(Long id, TipoDoc tipodoc, String numero) {
		this.id = id;
		this.tipodoc = tipodoc;
		this.numero = numero;	
	}
	
	public Documento(Long id, TipoDoc tipodoc, String numero, Cliente cliente) {
		this.id = id;
		this.tipodoc = tipodoc;
		this.numero = numero;
		this.cliente = cliente;
	}
	public Documento(Long id, String numero, Cliente cliente) {
		this.id = id;
		this.numero = numero;
		this.cliente = cliente;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoDoc getTipodoc() {
		return tipodoc;
	}

	public void setTipodoc(TipoDoc tipodoc) {
		this.tipodoc = tipodoc;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	

}
