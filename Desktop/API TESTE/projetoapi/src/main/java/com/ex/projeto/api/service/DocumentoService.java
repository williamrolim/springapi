package com.ex.projeto.api.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ex.projeto.api.model.Documento;
import com.ex.projeto.api.model.enums.TipoDoc;
import com.ex.projeto.api.repository.DocumentoRepository;
@Service
public class DocumentoService {
	
	@Autowired
	private DocumentoRepository documentoRepository;
	
	@Transactional
	public Documento saved(Documento documento) {
		Documento doc = documentoRepository.save(documento);
		return doc;
	}
	
}
