package com.ex.projeto.api.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ex.projeto.api.model.Cliente;
import com.ex.projeto.api.repository.ClienteRepository;
import com.ex.projeto.api.repository.DocumentoRepository;
import com.ex.projeto.api.repository.EnderecoRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;
//	@Autowired
//	private EnderecoRepository enderecoRepository;
//	@Autowired
//	private DocumentoRepository documentoRepository;
	
	@Transactional
	public Cliente saved(Cliente cliente) {
		Cliente clientes = clienteRepository.save(cliente);
		return clientes;
	}
	
}
