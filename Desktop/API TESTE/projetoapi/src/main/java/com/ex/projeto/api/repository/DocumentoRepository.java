package com.ex.projeto.api.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ex.projeto.api.model.Documento;
import com.ex.projeto.api.model.enums.TipoDoc;


public interface DocumentoRepository extends JpaRepository<Documento, Long> {

	@Query(value = "INSERT INTO Documento (tipodoc,numero) VALUES (:tipoDoc,:numero)", nativeQuery = true)
	@Transactional
	Documento inserirDocumento(@Param("tipoDoc") TipoDoc tipoDoc, @Param("numero") String numero);
}
