package com.ex.projeto.api.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.ex.projeto.api.model.Documento;
import com.ex.projeto.api.model.enums.TipoDoc;
import com.ex.projeto.api.service.DocumentoService;

@RestController
@RequestMapping("/api/v1/documentos")
public class DocumentoController {
	
	@Autowired
	private DocumentoService documentoService;
	
	@PostMapping
	public ResponseEntity<Documento>createDocumento(@RequestBody Documento documento){
		Documento docs = documentoService.saved(documento);
		URI locationURL = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(docs.getId())
				.toUri();
		return ResponseEntity.created(locationURL).build();
	}
	
//	@PostMapping
//	public void createDocumento2(@RequestParam("tipoDoc") TipoDocs tipoDoc, @RequestParam("numero") String numero){
//		Documento docs = documentoService.saved2(tipoDoc, numero);
//		URI locationURL = ServletUriComponentsBuilder
//				.fromCurrentRequest()
//				.path("/{id}")
//				.buildAndExpand(docs.getId())
//				.toUri();
//	}
}
