package spring.api.rest.springBootAPIRESTConstruaAPI.controller.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import spring.api.rest.springBootAPIRESTConstruaAPI.model.Topico;

public class TopicoDTO {
	private Long id;
	private String titulo;
	private String mensage;
	private LocalDateTime dataCriacao;
	
	public TopicoDTO(Topico topico) {//não preciso dos getters pois passo o objeto aqui, e atravez do topico preenche os atributos
		this.id = topico.getId();
		this.titulo = topico.getTitulo();
		this.mensage = topico.getMensagem();
		this.dataCriacao = topico.getDataCriacao();
	}

	public Long getId() {
		return id;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getMensage() {
		return mensage;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	//metodo recebe a lista de topicos e devolve uma lista de topicos DTO
	public static List<TopicoDTO> converter(List<Topico> topicos) {
		// (TopicoDTO::new) = chama o construtor que recebe o proprio topico como parametro
		//pra cada um ele chama o construtor , passando o topico como parametro
		//
		return topicos.stream().map(TopicoDTO::new).collect(Collectors.toList());
	}

	
}
