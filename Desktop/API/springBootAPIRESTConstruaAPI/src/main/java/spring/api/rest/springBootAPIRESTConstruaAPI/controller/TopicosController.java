package spring.api.rest.springBootAPIRESTConstruaAPI.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import spring.api.rest.springBootAPIRESTConstruaAPI.model.Curso;
import spring.api.rest.springBootAPIRESTConstruaAPI.model.Topico;

@Controller
public class TopicosController {
	
	@RequestMapping("/topicos")
	@ResponseBody // para avisar para o spring que não é uma aplicação web tradicional (vou só devolver) pega o retorno do metodo e devolve diretamente com navegador
	public List <Topico>lista(){
		//Spring usa uma biblioteca chamada Jackson , o jackson que faz a conversão de java para json. 
		//O spring usa o Jackson por baixo dos panos, 
		Topico topico = new Topico("Duvida", "Duvida com Spring", new Curso("Spring","programação"));
		return  Arrays.asList(topico,topico,topico);//varios objetos separados por virgula e ele tranforma em uma lista e devolve uma lista com todos os objetos 
	}
}
