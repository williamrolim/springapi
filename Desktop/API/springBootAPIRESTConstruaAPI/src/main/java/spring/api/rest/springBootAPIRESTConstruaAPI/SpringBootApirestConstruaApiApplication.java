package spring.api.rest.springBootAPIRESTConstruaAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootApirestConstruaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootApirestConstruaApiApplication.class, args);
	}

}
